using HTTPUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequestsTest : MonoBehaviour
{
    public string testEndpointURL;
    public string testBody;
    public List<KeyValueItem> headersList;
    public List<KeyValueItem> postFormItems;
    public string multipartFilePath;


    public void ExecuteGetRequestTest()
    {
        // Create request data
        HTTPRequestData requestData = new HTTPRequestData(testEndpointURL);

        // Add request headers
        foreach (var element in headersList)
        {
            requestData.AddHeader(element.Key, element.Value);
        }

        // Create request
        HTTPRequestGet requestGet = new HTTPRequestGet(requestData);

        // Add callbacks
        requestGet.onFinish += (requestGet, HTTPResponseData) =>
        {
            Debug.Log("Request Finished: \n " + requestData.ToDebugString());
        };

        requestGet.onFinishSuccess += (requestGet, HTTPResponseData) =>
        {
            Debug.Log("Finished Success: \n" + requestData.ToDebugString());
        };

        requestGet.onFinishFail += (requestGet, HTTPResponseData) =>
        {
            Debug.LogError("Finished Fail \n" + requestData.ToDebugString());
        };

        // ExecuteRequest
        requestGet.Exectute();


    }

    public void ExecutePostRequestURLEncodedTest()
    {
        HTTPRequestData requestData = new HTTPRequestData(testEndpointURL);

        foreach (var element in headersList)
        {
            requestData.AddHeader(element.Key, element.Value);
        }

        var body = HTTPRequestBody.CreateUrlEncodedFormBody();

        foreach (var element in postFormItems)
        {
            body.formFields.Add(element.Key, element.Value);
        }

        requestData.SetBody(body);

        HTTPRequestPost requestPost = new HTTPRequestPost(requestData);

        requestPost.onFinish += (requestGet, HTTPResponseData) =>
        {
            Debug.Log("Request Finished: \n " + requestData.ToDebugString());
        };

        requestPost.onFinishSuccess += (requestGet, HTTPResponseData) =>
        {
            Debug.Log("Finished Success: \n" + requestData.ToDebugString());
        };

        requestPost.onFinishFail += (requestGet, HTTPResponseData) =>
        {
            Debug.LogError("Finished Fail \n" + requestData.ToDebugString());
        };

        requestPost.Exectute();
    }


    public void ExecutePostRequestMultipartFormTest()
    {
        HTTPRequestData requestData = new HTTPRequestData(testEndpointURL);
        
        var body = HTTPRequestBody.CreateMultipartFormBody();
        body.AddFileSectionBytes(multipartFilePath);
        requestData.SetBody(body);

        HTTPRequestPost requestPost = new HTTPRequestPost(requestData);

        requestPost.onFinish += (requestGet, HTTPResponseData) =>
        {
            Debug.Log("Request Finished: \n " + requestData.ToDebugString());
        };

        requestPost.onFinishSuccess += (requestGet, HTTPResponseData) =>
        {
            Debug.Log("Finished Success: \n" + requestData.ToDebugString());
        };

        requestPost.onFinishFail += (requestGet, HTTPResponseData) =>
        {
            Debug.LogError("Finished Fail \n" + requestData.ToDebugString());
        };

        requestPost.Exectute();
    }

    [Serializable]
    public class KeyValueItem
    {
        public string Key;
        public string Value;
    }
}
