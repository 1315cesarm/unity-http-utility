using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace HTTPUtility
{
    public class HTTPResponseData
    {
        internal enum HTTPResponseResult
        {
            UNKNOWN                 = 0,
            IN_PROGRESS             = 1,
            SUCCESS                 = 2,
            CONNECTION_ERROR        = 3,
            PROTOCOL_ERROR          = 4,
            DATA_PROCESSING_ERROR   = 5
        }

        internal HTTPResponseResult requestResult;
        internal HTTPRequestData requestData;

        internal Dictionary<string, string> headers;
        internal int statusCode = -1;
        internal string data = null;
        internal string errorDescription = null;

        public HTTPResponseData(HTTPRequestData requestData, int statusCode = -1, string body = null, Dictionary<string, string> headers = null)
        {
            this.headers = (headers != null) ? headers : new Dictionary<string, string>();
            this.statusCode = statusCode;
            this.data = body;
            this.requestData = requestData;
        }

        public HTTPRequestData GetRequest()
        {
            return requestData;
        }

        public Dictionary<string, string> GetHeaders()
        {
            return new Dictionary<string, string>(headers);
        }

        public int GetStatusCode()
        {
            return statusCode;
        }

        public string GetBody()
        {
            return data;
        }
        internal void ClearData()
        {
            requestResult = HTTPResponseResult.UNKNOWN;
            requestData = null;
            headers = new Dictionary<string, string>();
            statusCode = -1;
            data = null;
        }

        public string ToDebugString()
        {
            string result = "";
            result += requestData.ToDebugString();
            result += "RESPONSE DATA\n";
            result += "\tRequest Result: " + this.requestResult.ToSafeString() + "\n";
            result += "\tStatus Code: " + this.statusCode.ToSafeString() + "\n";
            if(errorDescription != null)
            {
                result += "\tError description: " + this.errorDescription.ToSafeString() + "\n";
            }
            result += "\tStatus Code: " + this.statusCode.ToSafeString() + "\n";
            result += "\tHeaders: \n";
            foreach (var header in this.headers)
            {
                result += "\t\t" + header.Key.ToSafeString() + ": " + header.Value.ToSafeString() + "\n";
            }
            result += "\tData:\n" + this.data.ToSafeString() + "\n";
            return result;

        }

    }
}

