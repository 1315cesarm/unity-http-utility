using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HTTPUtility
{
    public class HTTPRequestManager
    {
        private int maxConcurrentRequests;

        private Queue<HTTPRequestBase> requestQueue;

        private List<HTTPRequestBase> activeRequests;

        private List<HTTPRequestBase> successRequests;

        private List<HTTPRequestBase> failedRequests;

        public bool performDownloads = false;

        public HTTPRequestManager(int maxConcurrentDownloads = 1)
        {
            this.requestQueue = new Queue<HTTPRequestBase>();
            this.activeRequests = new List<HTTPRequestBase>();
            this.successRequests = new List<HTTPRequestBase>();
            this.failedRequests = new List<HTTPRequestBase>();

            this.maxConcurrentRequests = maxConcurrentDownloads;
        }

        public void AddHTTPRequest(HTTPRequestBase httpRequest)
        {
            requestQueue.Enqueue(httpRequest);
        }

        public void ExecuteRequests()
        {
            while(requestQueue.Count > 0 && activeRequests.Count < maxConcurrentRequests)
            {
                HTTPRequestBase httpRequest = requestQueue.Dequeue();

                SetRequestListeners(httpRequest);

                activeRequests.Add(httpRequest);

                httpRequest.Exectute();
            }
        }

        internal void OnRequestSuccess(HTTPRequestBase httpRequest, HTTPResponseData httpRespnseData)
        {
            ClearRequestListeners(httpRequest);
            activeRequests.Remove(httpRequest);
            successRequests.Add(httpRequest);
            ExecuteRequests();

        }

        internal void OnRequestFail(HTTPRequestBase httpRequest, HTTPResponseData httpRespnseData)
        {
            ClearRequestListeners(httpRequest);
            activeRequests.Remove(httpRequest);
            failedRequests.Add(httpRequest);
            ExecuteRequests();
        }

        public void ResetFailedRequests(HTTPRequestBase httpRequest, HTTPResponseData httpRespnseData)
        {
            foreach(var request in failedRequests)
            {
                requestQueue.Enqueue(request);
            }
            failedRequests.Clear();
        }

        private void ClearRequestListeners(HTTPRequestBase httpRequest)
        {
            httpRequest.onFinishSuccess -= OnRequestSuccess;
            httpRequest.onFinishFail -= OnRequestSuccess;
        }

        private void SetRequestListeners(HTTPRequestBase httpRequest)
        {
            httpRequest.onFinishSuccess += OnRequestSuccess;
            httpRequest.onFinishFail += OnRequestSuccess;
        }

        public void SetMaxConcurrentDownloads(int value)
        {
            if(value <= 0)
            {
                value = 1;
            }
            this.maxConcurrentRequests = value;
        }

        public void Abort()
        {

        }

    }
}

