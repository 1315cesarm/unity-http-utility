using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(RequestsTest))]
public class RequestsTest_Editor : Editor
{
    SerializedProperty headersList;
    SerializedProperty postFormItems;
    private void OnEnable()
    {
        headersList = serializedObject.FindProperty("headersList");
        postFormItems = serializedObject.FindProperty("postFormItems");
    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();


        //serializedObject.ApplyModifiedProperties();

        RequestsTest script = (RequestsTest)target;

        

        

        EditorGUILayout.LabelField("Server settings", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        {
            
            
            EditorGUILayout.LabelField("URL:", EditorStyles.boldLabel);
            script.testEndpointURL = EditorGUILayout.TextField(script.testEndpointURL);

            if (string.IsNullOrEmpty(script.testEndpointURL))
            {
                EditorGUILayout.HelpBox("Is recomended to use https://webhook.site to setup a disposable test server.", MessageType.Error);
            }
            EditorGUILayout.PropertyField(headersList, new GUIContent("Request Headers"), true);
        }      
        EditorGUI.indentLevel--;

        EditorGUILayout.Space(20);

        

        EditorGUILayout.BeginVertical("box");
        {
            EditorGUILayout.LabelField("GET Request", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            if (GUILayout.Button("Execute"))
            {
                script.ExecuteGetRequestTest();
            }
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space(20);

        

        EditorGUILayout.BeginVertical("box");
        {
            EditorGUILayout.LabelField("Post Form URL encoded Request", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(postFormItems, new GUIContent("Form Items"), true);

            if (GUILayout.Button("Execute"))
            {
                script.ExecutePostRequestURLEncodedTest();
            }
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space(20);

        EditorGUILayout.BeginVertical("box");
        {
            EditorGUILayout.LabelField("Post Multipart", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.LabelField("FilePath:", EditorStyles.boldLabel);
            script.multipartFilePath = EditorGUILayout.TextField(script.multipartFilePath);

            if (GUILayout.Button("Execute"))
            {
                script.ExecutePostRequestMultipartFormTest();
            }
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space(20);

        EditorGUILayout.BeginVertical("box");
        {
            EditorGUILayout.LabelField("Clear Data", EditorStyles.boldLabel);
            if (GUILayout.Button("Clear Data"))
            {
                script.headersList.Clear();
                script.postFormItems.Clear();
            }
        }
        EditorGUILayout.EndVertical();


        serializedObject.ApplyModifiedProperties();

        

    }
}
