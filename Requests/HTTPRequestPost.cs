using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using static HTTPUtility.HTTPResponseData;

namespace HTTPUtility
{
    public class HTTPRequestPost : HTTPRequestBase
    {
        UnityWebRequest request;
        UnityWebRequestAsyncOperation operation;


        public HTTPRequestPost(HTTPRequestData requestData)
        {
            this.requestData = requestData;
            CreateRequest();
        }
        internal override void Abort()
        {
            this.state = State.ABORTED;
            request.Abort();
        }

        internal override void CreateRequest()
        {

            if (requestData.HasBody())
            {
                HTTPRequestBody body = requestData.GetBody();
                switch (body.GetRequestBodyType())
                {
                    case HTTPRequestBody.BodyType.UnityWWWForm:

                        HTTPUnityWWWFormBody unityWWWFormBody = (HTTPUnityWWWFormBody)body;
                        this.request = UnityWebRequest.Post(requestData.GetUri(), unityWWWFormBody.GetWWWForm());
                        break;

                    case HTTPRequestBody.BodyType.MultipartForm:
                        HTTPMultipartFormBody multipartFormBody = (HTTPMultipartFormBody)body;
                        this.request = UnityWebRequest.Post(requestData.GetUri(), multipartFormBody.GetMultipartFormData());
                        break;

                    case HTTPRequestBody.BodyType.UrlEncodedForm:
                        HTTPUrlEncodedFormBody urlEncodedFormBody = (HTTPUrlEncodedFormBody)body;
                        this.request = UnityWebRequest.Post(requestData.GetUri(), urlEncodedFormBody.GetUrlEncodedFormFields());
                        break;

                }
            }

            foreach (var element in requestData.GetHeaders())
            {
                this.request.SetRequestHeader(element.Key, element.Value);
            }

            this.responseData = new HTTPResponseData(requestData);
        }

        internal override void Exectute()
        {
            Debug.Log("EXECUTE() " + requestData.ToDebugString());

            responseData.ClearData();
            responseData.requestData = this.requestData;
            responseData.requestResult = HTTPResponseData.HTTPResponseResult.IN_PROGRESS;

            this.state = State.EXECUTING;
            this.operation = this.request.SendWebRequest();
            this.operation.completed += OnCompleted;
        }

        internal override void OnCompleted(AsyncOperation asyncOperation)
        {
            responseData.requestResult = (HTTPResponseResult)(request.result + 1);
            this.operation.completed -= this.OnCompleted;

            if ((int)request.result > 1)
            {
                this.OnFailure();
            }
            else
            {
                this.OnSuccess();
            }
        }

        internal override void OnFailure()
        {
            this.state = State.FINISHED_FAIL;

            if (request.GetResponseHeaders() != null)
            {
                responseData.headers = request.GetResponseHeaders();
            }

            if (request.responseCode != default)
            {
                responseData.statusCode = (int)request.responseCode;
            }

            if (request.downloadHandler.text != null)
            {
                responseData.data = request.downloadHandler.text;
            }

            if (request.error != null)
            {
                responseData.errorDescription = request.error;
            }

            this.onFinish?.Invoke(this, responseData);
            this.onFinishFail?.Invoke(this, responseData);
            Debug.LogError(responseData.ToDebugString());
        }

        internal override void OnSuccess()
        {
            this.state = State.FINISHED_SUCCESS;

            if (request.GetResponseHeaders() != null)
            {
                responseData.headers = request.GetResponseHeaders();
            }

            if (request.responseCode != default)
            {
                responseData.statusCode = (int)request.responseCode;
            }

            if (request.downloadHandler.text != null)
            {
                responseData.data = request.downloadHandler.text;
            }

            this.onFinish?.Invoke(this, responseData);
            this.onFinishSuccess?.Invoke(this, responseData);

            Debug.Log(responseData.ToDebugString());
        }
    }
}

