using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using static HTTPUtility.HTTPMultipartFormBody;

namespace HTTPUtility
{
    public abstract class HTTPRequestBody
    {
        public enum BodyType
        {
            UnityWWWForm,
            UrlEncodedForm, 
            MultipartForm
        }

        public abstract BodyType GetRequestBodyType();
    

        public static HTTPUnityWWWFormBody CreateUnityWWWFormBody(Dictionary<string, string> form)
        {
            HTTPUnityWWWFormBody result = new HTTPUnityWWWFormBody();
            result.formFields = form;
            return result;
        }

        public static HTTPMultipartFormBody CreateMultipartFormBody()
        {
            HTTPMultipartFormBody result = new HTTPMultipartFormBody();
            return result;
        }

        public static HTTPUrlEncodedFormBody CreateUrlEncodedFormBody()
        {
            HTTPUrlEncodedFormBody result = new HTTPUrlEncodedFormBody();
            return result;
        }
    }


    public class HTTPUnityWWWFormBody : HTTPRequestBody
    {
        public Dictionary<string, string> formFields = new Dictionary<string, string>();

        public override BodyType GetRequestBodyType()
        {
            return BodyType.UnityWWWForm;
        }

        public WWWForm GetWWWForm()
        {
            WWWForm wwwForm = new WWWForm();

            foreach (var element in formFields)
            {
                wwwForm.AddField(element.Key, element.Value);
            }

            return wwwForm;
        }
    }


    public class HTTPUrlEncodedFormBody : HTTPRequestBody
    {
        public Dictionary<string, string> formFields = new Dictionary<string, string>();

        public override BodyType GetRequestBodyType()
        {
            return BodyType.UrlEncodedForm;
        }

        public Dictionary<string, string> GetUrlEncodedFormFields()
        {
            return formFields;
        }
    }

    public class HTTPMultipartFormBody : HTTPRequestBody
    {
        List<IMultipartFormSection> multipartFormData = new List<IMultipartFormSection>();

        public override BodyType GetRequestBodyType()
        {
            return BodyType.MultipartForm;
        }

        public void AddFileSectionBytes(string absoluteFilePath)
        {
            string filename = Path.GetFileName(absoluteFilePath);
            byte[] data = File.ReadAllBytes(absoluteFilePath);

            multipartFormData.Add(new MultipartFormFileSection(filename, data));              
        }

        public void AddFileSectionBytes(string absoluteFilePath, string contentType, string name)
        {
            string filename = Path.GetFileName(absoluteFilePath);
            byte[] data = File.ReadAllBytes(absoluteFilePath);

            multipartFormData.Add(new MultipartFormFileSection(name, data, filename, contentType));
        }

        public void AddFileSectionString(string fileName, string data)
        {
            multipartFormData.Add(new MultipartFormFileSection(data, fileName));
        }

        public void AddFileSectionString(string fileName, string data, Encoding dataEncoding)
        {
            multipartFormData.Add(new MultipartFormFileSection(data, dataEncoding, fileName));
        }

        public void AddFileSectionString(string sectionName, string fileName, string data, Encoding dataEncoding)
        {
            multipartFormData.Add(new MultipartFormFileSection(sectionName, data, dataEncoding, fileName));
        }

        public List<IMultipartFormSection> GetMultipartFormData()
        {
            return multipartFormData;
            
        }
    }
}