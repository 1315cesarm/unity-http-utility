# Notes
## Current Status
Working:
	
- Get calls
- Post calls
	- As a form	
	- As a multipart

	
In progress:
- Request Manager

## Examples

For running your own experiments, I recommend you to use https://webhook.site

### HTTP GET Request
```csharp
	HTTPRequestData requestData = new HTTPRequestData("http:// url");

	requestData.AddHeader("key1, value1");
	requestData.AddHeader("key2, value2");
	requestData.AddHeader("key3, value3");

	HTTPRequestGet requestGet = new HTTPRequestGet(requestData);

	requestGet.onFinish += (requestGet, HTTPResponseData) =>
	{
		Debug.Log("Request Finished: \n " + requestData.ToDebugString());
	};

	requestGet.onFinishSuccess += (requestGet, HTTPResponseData) =>
	{
		Debug.Log("Finished Success: \n" + requestData.ToDebugString());
	};

	requestGet.onFinishFail += (requestGet, HTTPResponseData) =>
	{
		Debug.LogError("Finished Fail \n" + requestData.ToDebugString());
	};

	// ExecuteRequest
	requestGet.Exectute();

```

### HTTP POST Url Encoded Form

```csharp
	HTTPRequestData requestData = new HTTPRequestData("http:// url");
	
	requestData.AddHeader("key1, value1");
	requestData.AddHeader("key2, value2");
	requestData.AddHeader("key3, value3");
	

	var body = HTTPRequestBody.CreateUrlEncodedFormBody();

	body.formFields.Add("formKey1", "formValue1");
	body.formFields.Add("formKey2", "formValue2");
	body.formFields.Add("formKey3", "formValue3");

	requestData.SetBody(body);

	HTTPRequestPost requestPost = new HTTPRequestPost(requestData);

	requestPost.onFinish += (requestGet, HTTPResponseData) =>
	{
		Debug.Log("Request Finished: \n " + requestData.ToDebugString());
	};

	requestPost.onFinishSuccess += (requestGet, HTTPResponseData) =>
	{
		Debug.Log("Finished Success: \n" + requestData.ToDebugString());
	};

	requestPost.onFinishFail += (requestGet, HTTPResponseData) =>
	{
		Debug.LogError("Finished Fail \n" + requestData.ToDebugString());
	};

	requestPost.Exectute();
```

### HTTP POST MultiPart (file upload)

```csharp

	HTTPRequestData requestData = new HTTPRequestData("http:// url");
        
	var body = HTTPRequestBody.CreateMultipartFormBody();
	body.AddFileSectionBytes("filepath");
	requestData.SetBody(body);

	HTTPRequestPost requestPost = new HTTPRequestPost(requestData);

	requestPost.onFinish += (requestGet, HTTPResponseData) =>
	{
		Debug.Log("Request Finished: \n " + requestData.ToDebugString());
	};

	requestPost.onFinishSuccess += (requestGet, HTTPResponseData) =>
	{
		Debug.Log("Finished Success: \n" + requestData.ToDebugString());
	};

	requestPost.onFinishFail += (requestGet, HTTPResponseData) =>
	{
		Debug.LogError("Finished Fail \n" + requestData.ToDebugString());
	};

	requestPost.Exectute();

```