using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace HTTPUtility
{
    public abstract class HTTPRequestBase
    {
        protected enum State { READY, EXECUTING, FINISHED_SUCCESS, FINISHED_FAIL, ABORTED }



        protected State state;

        internal HTTPRequestData requestData = null;
        internal HTTPResponseData responseData = null;

        public Action<HTTPRequestBase, HTTPResponseData> onExecuteStart;
        public Action<HTTPRequestBase, HTTPResponseData> onFinish;
        public Action<HTTPRequestBase, HTTPResponseData> onFinishSuccess;
        public Action<HTTPRequestBase, HTTPResponseData> onFinishFail;
        
        internal abstract void CreateRequest();

        internal abstract void Exectute();

        internal abstract void Abort();

        internal abstract void OnCompleted(AsyncOperation asyncOperation);

        internal abstract void OnSuccess();

        internal abstract void OnFailure();

    }
}