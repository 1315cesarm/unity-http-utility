using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Networking;
using static HTTPUtility.HTTPResponseData;

namespace HTTPUtility
{
    public class HTTPRequestGet : HTTPRequestBase
    {
        
        UnityWebRequest request;
        UnityWebRequestAsyncOperation operation;

        
        public HTTPRequestGet(HTTPRequestData requestData)
        {
            this.requestData = requestData;
            CreateRequest();
        }

        internal override void Abort()
        {
            this.state = State.ABORTED;
            request.Abort();

        }

        internal override void CreateRequest()
        {
            this.request = UnityWebRequest.Get(requestData.GetUri());
            foreach (var element in requestData.GetHeaders())
            {
                this.request.SetRequestHeader(element.Key, element.Value);
            }
            this.state = State.READY;
            this.responseData = new HTTPResponseData(requestData);
        }

        internal override void Exectute()
        {
            Debug.Log("EXECUTE() " + requestData.ToDebugString());
            if (state == State.EXECUTING) return;

            responseData.ClearData();
            responseData.requestData = this.requestData;
            responseData.requestResult = HTTPResponseData.HTTPResponseResult.IN_PROGRESS;

            this.state = State.EXECUTING;
            this.operation = this.request.SendWebRequest();
            this.operation.completed += OnCompleted;

        }

        internal override void OnCompleted(AsyncOperation asyncOperation)
        {
            responseData.requestResult = (HTTPResponseResult)(request.result + 1);
            this.operation.completed -= this.OnCompleted;
            
            if ((int)request.result > 1)
            {
                this.OnFailure();
            }
            else
            {
                this.OnSuccess();
            }
        }

        internal override void OnSuccess()
        {
            this.state = State.FINISHED_SUCCESS;

            if (request.GetResponseHeaders() != null)
            {
                responseData.headers = request.GetResponseHeaders();
            }

            if (request.responseCode != default)
            {
                responseData.statusCode = (int)request.responseCode;
            }

            if (request.downloadHandler.text != null)
            {
                responseData.data = request.downloadHandler.text;
            }

            this.onFinish?.Invoke(this, responseData);
            this.onFinishSuccess?.Invoke(this, responseData);

            Debug.Log(responseData.ToDebugString());
            
        }

        internal override void OnFailure()
        {
            this.state = State.FINISHED_FAIL;

            if (request.GetResponseHeaders() != null)
            {
                responseData.headers = request.GetResponseHeaders();
            }

            if (request.responseCode != default)
            {
                responseData.statusCode = (int)request.responseCode;
            }

            if(request.downloadHandler.text != null)
            {
                responseData.data = request.downloadHandler.text;
            }

            if(request.error != null)
            {
                responseData.errorDescription = request.error;
            }

            this.onFinish?.Invoke(this, responseData);
            this.onFinishFail?.Invoke(this, responseData);
            Debug.LogError(responseData.ToDebugString());
        }
    }
}

