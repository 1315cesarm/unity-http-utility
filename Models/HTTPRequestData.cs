using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace HTTPUtility
{
    public class HTTPRequestData
    {
        protected Uri uri;
        protected Dictionary<string, string> headers;
        protected HTTPRequestBody body;
        


        public HTTPRequestData(string uri)
        {
            this.uri = new Uri(uri);
            this.headers = new Dictionary<string, string>();
            this.body = null;
        }
        #region uri

        /// <summary>
        /// Returns the uri of the request data.
        /// </summary>
        /// <returns></returns>
        public Uri GetUri()
        {
            return uri;
        }

        #endregion

        #region headers
        /// <summary>
        /// Checks if request data contains a certain header.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal bool ContainsHeader(string key)
        {
            if (this.headers.ContainsKey(key))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds a header to the request data.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="overwrite">If header key exist, it's value will be overwritten.</param>
        internal void AddHeader(string key, string value, bool overwrite = false)
        {
            if (ContainsHeader(key) && overwrite)
            {
                this.headers[key] = value;
            }
            else
            {
                this.headers.Add(key, value);
            }
        }

        /// <summary>
        /// Returns a dictionary containing all the headers.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetHeaders()
        {
            return new Dictionary<string, string>(headers);
        }

        #endregion

        #region body

        /// <summary>
        /// Sets the body of the request data.
        /// </summary>
        /// <param name="body"></param>
        internal void SetBody(HTTPRequestBody body)
        {
            this.body = body;
        }

        public HTTPRequestBody GetBody()
        {
            return this.body;
        }
        /// <summary>
        /// Checks if request data contains a body.
        /// </summary>
        /// <returns>True if body is not null.</returns>
        public bool HasBody()
        {
            if (this.body == null)
            {
                return false;
            }
            return true;
        }

        #endregion

        public string ToDebugString()
        {
            string result = "";
            result += "REQUEST DATA\n";
            result += "\tUri: " + this.uri.ToSafeString() + "\n";
            result += "\tHeaders: \n";
            foreach(var header in this.headers)
            {
                result += "\t\t" + header.Key.ToSafeString() + ": " + header.Value.ToSafeString() + "\n";
            }
            result += "\tBody: " + this.body.ToSafeString() + "\n";
            return result;

        }
    }
}

